#Simulation sickness.

From what I have found avoiding manipulation of the camera is best as for some people certain movements can cause sickness. 
Moving via the track pad is a very easy way to make most people feel nauseous. I have avoided this in my project by only being able to move by teleportation. 
Taking away the players ability to move can cause a bad vr experience.   

[source](https://madewith.unity.com/stories/development-update-7-exploring-the-virtual).

 
#Text and Image Legibility

In some cases, with VR you can get problems with text/image rendering resolution plays a large role in this in some cases only the text in the middle of your field of view would be clear.
This can be avoided by using scroll able columns instead of long lines of text. In my case with there being so little assets in the scene
I was able to just use large text on a solid background facing the player without it being an issue.

[source](https://medium.com/@LeapMotion/vr-design-best-practices-bb889c2dc70).

#User Interface
When it comes to user interface it is good practice to not make the player to swivel their eyes in their sockets just to see the UI this can be avoided by keeping
the UI in the middle 1/3rd of the players viewing area giving them the ability to examine the UI with head movements.

[source](https://developer.oculus.com/design/latest/concepts/bp_intro/).

#Do not attach things to the camera.

This can be unpleasant for the user being unable to look away from something that it attached to your camera this could be fixed by having popup menus that can be closed when the user is finished making changes or reading.

[source](https://virtualrealitypop.com/practical-vr-ce80427e8e9d).


#Environment/Grounding.
You want to keep the user on the ground this involves creating a floor so users have the feeling they are on solid ground not just floating around the map.
I have achieved this in my map by making a meshed floor that the user can teleport around on.

[source](https://virtualrealitypop.com/practical-vr-ce80427e8e9d).

#The sky / background.
If you are looking to have a 3d environment you will need to have a sky-box this is more or less a background for your terrain this consists of a cube with a texture that is 6 parts folded onto the inside of the cube.
I didn't have any need for a sky box in my application but in any future games it will be a necessity to make it feel complete.

[source](https://virtualrealitypop.com/practical-vr-ce80427e8e9d).

#User Testing.

User testing is very important in any app/game development testing early and often with a wide verity of people. Using people who have experience in vr is good but also people
who have little use or even being a first time user is very helpful. Gathering information from users can find bugs or problems that you could have missed also making sure what you have made is easy to use and makes sense.

[source](https://virtualrealitypop.com/practical-vr-ce80427e8e9d).

#Height Matching

Height Matching can really make a game feel complete and immersive. Setting the height of the camera to suit the type of person that will be using the app/game
can help to make it feel like they are really standing there not just in a simulation. 

[source](https://virtualrealitypop.com/practical-vr-ce80427e8e9d).


#Peer Review from Taylor#

Two of Marks points for good practises for GUI programming were having instructions as to help be user friendly and placing desired items infront of you for the VR programming so that the user
doesn't have to look around to find what you want them to do. He placed the instructions of how to change the spinning boxes color and the sliders required to do so infront of you so i didn't have to look for them.
Overall he made the whole experience very user friendly which made the whole experience enjoyable.
